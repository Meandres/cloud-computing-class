# Projet of Embedded System developpement

## How to use

* `make run`
* to access the work of the part 1 please add `-DCONS` to the `CFLAGS` variable in the make file or rollback to commit `94615c2f963b4472796d69af7eb6963e4efe7e06`

## What is working

The basic console functions are working. The arrow keys are recognised but they are not working as expected (to get the history of the console).

The interrupts are set up. They work for the first 21 characters entered by the keyboard.

## What is not working

For the interrupt, something quite peculiar is happening. I can write as much as I want to the screen. However, when waiting for input from the keyboard only the first 21 characters are printed and then the system blocks. I have not found the reason for this. The exact position of the blocking is at line 69 of the following code excerpt :
```
61 void put_char(unsigned char s){
62     unsigned short* uart_fr = (unsigned short*) (UART0+UART_FR);
63     unsigned short* uart_dr = (unsigned short*) (UART0+UART_DR);
64     if(*uart_fr & UART_TXFF){
65         uart_send_string(UART1, "buffering please wait\r\n");
66         cb_put(tx_cb, s);
67     }else{
68         uart_send_string(UART1, "sending directly\r\n");
69         *uart_dr = s;
70     }
71     uart_send_string(UART1, "end of put_char function\r\n");
72 }
```
This can be verified when reading the output on the UART1 : it will print "sending directly" one last time and then will hang without printing "end of put\_char function".
