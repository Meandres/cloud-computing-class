# Journal

## Week 1

### Lecture 1

### Homework

Step 1:

Need to install newlib otherwise stdint.h is missing.

Trying to run qemu : permission denied. Arch wiki tells me I shouldn't run as root.
Found the problem. I didn't install the right package.

Machine runs but I can't stop it ^^. Ctrl+a then x stops the guest machine.

Sleeping is nice and all but I can't write in this console. Removing the print for now.

BSS section is to hold global variables that are not initialised in the program. In C standard, numerical variables are set to 0. Pointers to the null pointer. So the bss section is reset to 0.

I used arm-none-eabi-size to see the size of each section. At first, adding uninitialised variables didn't change the size of the bss section. I figured it was the compiler that was removing unused variables so I incremented them and then they appeared in the bss section.

The bss is 16-bytes aligned because arm requires this section to be 16 bytes aligned. Also the code in reset.s zeroes 16 bytes at a time.

Step 2 :

The boot sequence proceeds as follow :
- The memory is mapped following the layout in kernel.ld. The entry point specified (\_vector) is put at the beginning of the address space. The PC will start executing at @0. There it will branch to \_reset (as we can see in the disassembled code of the .vector section : 0 -> put #24 in pc, which then points to the handler of undefined\_instructions which points to \_reset).
- the code in reset.s is executed. It zeroes out the bss section, sets the stack pointer and then branches to \_start which is the first function in the C world.

The print of Zzz indicates that there is interruptions. The while loop in main.c loops endlessly.
 
As the stack grows "downwards" (toward lower addresses), the stack top needs to be set to the highest address reachable.

A way to verify that the stack is working properly is to call a function. If the stack works, calling and returning from a function should be possible smoothly.
 
Pushing too many frames on the stack could result in a stack overflow resulting in a segmentation fault.
Last year in the OS programming project, we implemented a guard against stack smashing by following the usual technique which is to add a padding between 2 stack frames which contains a number. This number is checked when removing the lower stack frame. If the number is not valid, then stack smashing happened and we would kill the process. However, I don't know what could be the right answer to a stack smashing of the kernel stack.

## Week 2
 
### Lecture 2
 
Now that we have our boot process. We can build on it.
 
One of the problems : there is no interuptions.

There is some time constraints to be respected on the hardware piloted by the OS.
Chipset that handles the interuptions. The devices communicate with the controller which itself communicate with the CPU.
2 bit-field registers, one to set and one to clear.

The focus from now on will be on the interrupt service routine. 
Interrupt service routine is triggered when the CPU jumps to the address @0+18 (hardware decided place).
The registers are saved and then the C routine is called.

But this only solves the spinning problems. Not the timing issue.
To solve this : buffering. Quickly save the data in a buffer, and process it later when there is time.
However there is a race condition with the buffer. To solve this : lock-free circular buffer. (side note : super weird. Gotta look into this).
This is not enough to get out of the timing issue. Its just buying a bit more time. 

We can use multiple levels of buffering to buy even more time.

Ok fine, but how do we organize the main loop.
Event-oriented programming. The \_start() function is transformed into a *event pump*. The loop sleeps until the next delayed event becomes ready. It needs a timer to be woken up. There is still a race condition with the ready and delayed queues. 

Not a very general design (only for the timer). To handle the other interrupts.

The design is made of two arrays. Tops and Bottoms. The tops are immediate handlers that deal with the interruptions. If the top doesn't have time to fully process the interrupt, it sets up a timer to call the related bottom. 
Example : for the serial line, the top simply reads the character on the circular buffer. The bottom will read the circular buffer and process it. 
