#include "console.h"

int history_index=0;
int history_index_search=0;
char history[20][128];

void run_console(){
    reset_screen();
    int new_line=true;
    int not_recognized;
    int new_char_pos=0;
    int index_argument;
    char line[128];
    char command[128];
    int buffer_index=0;
    while(1){
        unsigned char c;
        if(new_line==true) { kprintf("> "); new_line=false; }
        while(uart_receive(UART0, &c) == 0){}
        if(c == '\r'){
            new_line=true;
            index_argument=extract_command(line, command);
            not_recognized=true;
            kprintf("\n\r");
            if(str_cmp(command, "echo")==true){
                echo(&line[index_argument]);
                not_recognized=false;
            }
            if(str_cmp(command, "clear")==true){
                reset_screen();
                not_recognized=false;
            }
            if(not_recognized==true){
                echo("Unknown command : ");
                echo(command);
            }
            new_char_pos=0;
            store_line(line); 
        }else{
            switch(c){
                case 127:
                    new_char_pos= new_char_pos == 0 ? 0 : new_char_pos-1;
                    line[new_char_pos]='\0';
                    break;
                case 27:
                    if(buffer_index==0){
                        buffer_index++;
                    }else{
                        buffer_index=0;
                    }
                    break;
                case 91:
                    if(buffer_index==1){
                        buffer_index++;
                    }else{
                        buffer_index=0;
                    }
                    break;
                case 65:
                    if(buffer_index==2){
                        erase_character();
                        erase_character();
                        if((history_index_search-1)%20 != history_index){
                            if(history_index_search==0){
                                history_index_search=20;
                            }else{
                                history_index_search--;
                            }
                            kputchar('\r');
                            str_cpy(line, history[history_index_search]);
                        }
                        buffer_index=0;
                    }else{
                        buffer_index=0;
                    }
                    break; 
                case 66:
                    if(buffer_index==2){
                        erase_character();
                        erase_character();
                        if(history_index_search != history_index && (history_index_search+1)%20 != history_index){
                            history_index_search=(history_index_search+1)%20;
                            kputchar('\r');
                            str_cpy(line, history[history_index_search]);
                        }
                        buffer_index=0;
                    }else{
                        buffer_index=0;
                    }
                break; 
                default:
                    line[new_char_pos]=c;
                    new_char_pos++;
            }
        }
        if(c==127){
            erase_character();
        }else{
           if(buffer_index!=2){
               kputchar(c);
           } 
        }
    }
}

void reset_screen(){
    kprintf("\033[2J");
    kprintf("\033[0;0H");
}
void echo(char *s){
    kprintf(s);
    kprintf("\n\r");
}
void store_line(char *line){
    int i=0;
    while(i<128 && line[i]!='\0'){
        history[history_index][i]=line[i];
        line[i]='\0';
        i++;
    }
    history_index = (history_index + 1)%20;
    history_index_search = (history_index_search + 1)%20;
}

void erase_character(){
    kputchar('\b');
    kputchar(' ');
    kputchar('\b');
}

void print_char_code(int c){
    if (c>9){
        int n = c / 10;
        c -= n*10;
        print_char_code(n);
    }
    uart_send(UART1, '0'+c);
}

void load_line(char *line){
    for(int i=0; i<128; i++){
        erase_character();
    }
    kprintf(line);
}

// String helpers

int extract_command(char *line, char *command){ //writes the command to the given space on the stack and returns the index to the beginning of the argument(s)
     int i=0;
     while(line[i]!='\0' && line[i]!=' '){
         command[i]=line[i];
         i++;
     }
     command[i]='\0';
     return i+1;
}

int str_cmp(char *s1, char *s2){
    int i=0;
    while(s1[i] != '\0' && s2[i] != '\0' && s1[i] == s2[i]){
        i++;
    }
    if(s1[i]=='\0' || s2[i]=='\0'){
       if(s1[i]=='\0' && s2[i]=='\0'){ // end reached for both strings => equal
           return true;
       }else{ // only one of the strings has reached the end => not equal
        return false;
       }
    }else{ // neither string ended hence not equal.
        return false;
    }
} 
void str_cpy(char *src, char *dest){
    for(int i=0; i<128; i++){
        dest[i]=src[i];
    }
}

