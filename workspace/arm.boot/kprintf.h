#ifndef KPRINTF_H
#define KPRINTF_H

#include "main.h"
#include "handlers.h"

void kputchar(int c);
void kprintf(const char *fmt, ...);

#endif
