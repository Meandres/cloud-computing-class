#ifndef HANDLERS_H
#define HANDLERS_H

#include "main.h"
#include "uart.h"
#include "cb.h"

void init_handlers_cb();

void handlers_dispatch(void* cookie);
void handler_uart_rx(void* cookie);
void handler_uart_tx(void* cookie);
void handler_uart_timer(void* cookie);

int read_char(unsigned char*s);
void put_char(unsigned char s);

#endif
