#ifndef CONSOLE_H
#define CONSOLE_H

#define false 0
#define true 1
#include "main.h"
#include "kprintf.h"

void run_console();

// console functions
void echo(char *s);
void reset_screen();
void store_line(char *line);
void load_line(char *line);
void erase_character();
void print_char_code(int c);

// string helpers
int extract_command(char *line, char *command);
int str_cmp(char *s1, char *s2);
void str_cpy(char *src, char *dest);
#endif
