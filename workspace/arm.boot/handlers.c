#include "handlers.h"
#include "uart-irqs.h"

struct cb *rx_cb;
struct cb *tx_cb;

void handlers_dispatch(void* cookie){ //dispatch depending on the interrupt raised
    if(UART0+UART_MIS & UART_MIS_RXMIS){
        handler_uart_rx(cookie);
    }
    if(UART0+UART_MIS & UART_MIS_TXMIS){
        handler_uart_tx(cookie);
    }
    if(UART0+UART_MIS & UART_MIS_RTMIS){
        handler_uart_timer(cookie);
    }
    // clearing the interrupts
    unsigned short* uart_ICR = (unsigned short*) (UART0+UART_ICR);
    *uart_ICR |= UART_ICR_RTIC | UART_ICR_TXIC | UART_ICR_RXIC;
}

void init_handlers_cb(){
    cb_init(rx_cb);
    cb_init(tx_cb);
}

void handler_uart_rx(void* cookie){
    unsigned char c;
    int res = uart_receive(UART0, &c);
    while(res!=0 && !cb_full(rx_cb)){
        cb_put(rx_cb, c);
        res = uart_receive(UART0, &c);
    }
}


int read_char(unsigned char *s){
    return cb_get(rx_cb, s)+1;
}

void handler_uart_tx(void* cookie){
    unsigned char c;
    unsigned short* uart_fr = (unsigned short*) (UART0+UART_FR);
    while(!(*uart_fr & UART_TXFF) && !cb_empty(tx_cb)){
        cb_get(tx_cb, &c);
        uart_send(UART0, c);
    }
}

void handler_uart_timer(void* cookie){
    unsigned char c;
    int res = uart_receive(UART0, &c);
    while(res!=0){
        if(!cb_full(rx_cb)){
            cb_put(rx_cb, c);
        }
        res = uart_receive(UART0, &c);
    }
}

void put_char(unsigned char s){
    unsigned short* uart_fr = (unsigned short*) (UART0+UART_FR);
    unsigned short* uart_dr = (unsigned short*) (UART0+UART_DR);
    if(*uart_fr & UART_TXFF){
        uart_send_string(UART1, "buffering please wait\r\n");
        cb_put(tx_cb, s);
    }else{
        uart_send_string(UART1, "sending directly\r\n");
        *uart_dr = s;
    }
    uart_send_string(UART1, "end of put_char function\r\n");
}

