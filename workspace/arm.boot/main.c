#include "main.h"
#include "kprintf.h"
#include "console.h"
#include "vic.h"
#include "uart-irqs.h"
#include "handlers.h"

/**
 * This is the C entry point, upcalled once the hardware has been setup properly
 * in assembly language, see the reset.s file.
 */

void _start() {
    #ifndef CONS
    vic_setup();
    void *cookie=NULL;
    vic_irq_enable(UART0_IRQ, handlers_dispatch, cookie);
    init_handlers_cb(); 
    unsigned short* cuart_lcr = (unsigned short*) (UART0 + CUARTLCR_H);
    *cuart_lcr = CUARTLCR_H_FEN;
    unsigned short* uart_imsc = (unsigned short*) (UART0 + UART_IMSC);
    *uart_imsc = UART_IMSC_RXIM | UART_IMSC_TXIM | UART_IMSC_RTIM;
    vic_enable();

    kprintf("\nQuit with \"C-a c\" and then type in \"quit\".\nTrying to write a lot of text to verify that the transmitting buffer works up until now. Why it happens later is a mistery that I did not manage to solve.\r\n");
    unsigned char c;
    while(1){
        if(read_char(&c)==1){
            put_char(c);
        }
        wfi();
    }
    #else
    run_console();
    #endif
}
